#!/bin/sh

DATE=`date "+%d/%m/%Y"`

echo "This ONELAB bundle was built on ${DATE} with the latest builds of Gmsh
(https://gmsh.info) and GetDP (https://getdp.info).

To run your first simulation, launch Gmsh and open a GetDP .pro file
(e.g. models/Magnetometer/magnetometer.pro) with the File/Open menu, then click
on \"Run\".

The software is distributed under the terms of the GNU General Public License.
See the LICENSE and CREDITS files for more information.

The 'tutorials' directory contains ONELAB tutorials. The 'models' directory
contains a selection of other ready-to-use models from https://onelab.info. The
'templates' directory contains generic model templates.

See https://onelab.info for additional examples, up-to-date versions and
documentation." > /tmp/README.txt

KEYCHAIN="FIXME"

GMSH=git
GETDP=git
CONVEKS=git

#GMSH=4.0.1
#GETDP=3.0.1
#CONVEKS=1.0.0

# get models and tutorials
curl -O https://onelab.info/files/bundle_tutorials_getdp.txt
curl -O https://onelab.info/files/bundle_tutorials_cpp.txt
curl -O https://onelab.info/files/bundle_tutorials_python.txt
curl -O https://onelab.info/files/bundle_models.txt
TUTO_GETDP=`cat bundle_tutorials_getdp.txt`
TUTO_CPP=`cat bundle_tutorials_cpp.txt`
TUTO_PYTHON=`cat bundle_tutorials_python.txt`
MODELS=`cat bundle_models.txt`
rm -rf /tmp/models
mkdir /tmp/models
for m in ${MODELS}; do
  curl -O https://onelab.info/files/${m}.zip
  unzip -q -o ${m}.zip -d /tmp/models
  rm -f ${m}.zip
done
curl -O https://gitlab.onelab.info/life-hts/life-hts/-/archive/master/life-hts-master.zip
unzip -q -o life-hts-master.zip -d /tmp/models
mv /tmp/models/life-hts-master /tmp/models/Life-HTS
rm -f life-hts-master.zip

rm -rf /tmp/tutorials
mkdir /tmp/tutorials
mkdir /tmp/tutorials/gmsh
mkdir /tmp/tutorials/getdp
for m in ${TUTO_GETDP}; do
  curl -O https://onelab.info/files/${m}.zip
  unzip -q -o ${m}.zip -d /tmp/tutorials/getdp
  rm -f ${m}.zip
done
mkdir /tmp/tutorials/python
for m in ${TUTO_PYTHON}; do
  curl -O https://onelab.info/files/${m}.zip
  unzip -q -o ${m}.zip -d /tmp/tutorials/python
  rm -f ${m}.zip
done
mkdir /tmp/tutorials/c++
for m in ${TUTO_CPP}; do
  curl -O https://onelab.info/files/${m}.zip
  unzip -q -o ${m}.zip -d /tmp/tutorials/c++
  rm -f ${m}.zip
done
git clone https://gitlab.onelab.info/conveks/tutorials /tmp/tutorials/conveks
rm -rf /tmp/tutorials/conveks/.git

if [ $# -lt 1 ] || [ $1 == "source" ]; then
  rm -rf onelab-source*
  mkdir onelab-source
  curl -O https://gmsh.info/src/gmsh-${GMSH}-source.tgz
  curl -O https://getdp.info/src/getdp-${GETDP}-source.tgz
  mv gmsh-${GMSH}-source.tgz /tmp
  mv getdp-${GETDP}-source.tgz /tmp
  tar zxvf /tmp/gmsh-${GMSH}-source.tgz -C /tmp
  tar zxvf /tmp/getdp-${GETDP}-source.tgz -C /tmp
  cp /tmp/README.txt onelab-source
  mv /tmp/gmsh-*${GMSH}*-source onelab-source
  mv /tmp/getdp-*${GETDP}*-source onelab-source
  cp onelab-source/gmsh-*${GMSH}*-source/LICENSE.txt onelab-source/LICENSE.txt
  echo "\n\n" >> onelab-source/LICENSE.txt
  cat onelab-source/getdp-*${GETDP}*-source/LICENSE.txt >> onelab-source/LICENSE.txt
  cp onelab-source/gmsh-*${GMSH}*-source/CREDITS.txt onelab-source/CREDITS.txt
  echo "\n\n" >> onelab-source/CREDITS.txt
  cat onelab-source/getdp-*${GETDP}*-source/CREDITS.txt >> onelab-source/CREDITS.txt
  cp -R /tmp/models onelab-source/
  cp -R /tmp/tutorials onelab-source/
  cp -R onelab-source/gmsh-*${GMSH}*-source/tutorials/* onelab-source/tutorials/gmsh/
  cp -R onelab-source/getdp-*${GETDP}*-source/templates onelab-source/
  rm -rf /tmp/gmsh-*
  rm -rf /tmp/getdp-*
  zip -r onelab-source.zip onelab-source
  rm -rf onelab-source
  scp onelab-source.zip geuzaine@onelab.info:/onelab_files/
fi

if [ $# -lt 1 ] || [ $1 == "windows" ]; then
  rm -rf onelab-Windows64*
  mkdir onelab-Windows64
  curl -O https://gmsh.info/bin/Windows/gmsh-${GMSH}-Windows64.zip
  curl -O https://getdp.info/bin/Windows/getdp-${GETDP}-Windows64c.zip
  ##curl -O https://getdp.info/bin/Windows/getdp-${GETDP}-Windows64r.zip
  curl -O https://onelab.info/conveks/bin/conveks-${CONVEKS}-Windows64.zip
  mv gmsh-${GMSH}-Windows64.zip /tmp
  mv getdp-${GETDP}-Windows64c.zip /tmp
  ##mv getdp-${GETDP}-Windows64r.zip /tmp
  mv conveks-${CONVEKS}-Windows64.zip /tmp
  unzip -q -o /tmp/gmsh-${GMSH}-Windows64.zip -d /tmp
  unzip -q -o /tmp/getdp-${GETDP}-Windows64c.zip -d /tmp
  ##unzip -q -o /tmp/getdp-${GETDP}-Windows64r.zip -d /tmp
  unzip -q -o /tmp/conveks-${CONVEKS}-Windows64.zip -d /tmp
  cp /tmp/README.txt onelab-Windows64
  perl -pi -e 's/\n/\r\n/' onelab-Windows64/README.txt
  mv /tmp/gmsh-*${GMSH}-Windows64/gmsh.exe onelab-Windows64
  mv /tmp/gmsh-*${GMSH}-Windows64/onelab.py onelab-Windows64
  mv /tmp/getdp-*${GETDP}-Windows64/getdp.exe onelab-Windows64
  mv /tmp/conveks-*${CONVEKS}-Windows64/lib/conveks.py onelab-Windows64
  mv /tmp/conveks-*${CONVEKS}-Windows64/lib/conveks*.dll onelab-Windows64
  mv /tmp/gmsh-*${GMSH}-Windows64/LICENSE.txt onelab-Windows64/LICENSE.txt
  echo "\n\n" >> onelab-Windows64/LICENSE.txt
  cat /tmp/getdp-*${GETDP}-Windows64/LICENSE.txt >> onelab-Windows64/LICENSE.txt
  mv /tmp/gmsh-*${GMSH}-Windows64/CREDITS.txt onelab-Windows64/CREDITS.txt
  echo "\n\n" >> onelab-Windows64/CREDITS.txt
  cat /tmp/getdp-*${GETDP}-Windows64/CREDITS.txt >> onelab-Windows64/CREDITS.txt
  cp -R /tmp/models onelab-Windows64/
  cp -R /tmp/tutorials onelab-Windows64/
  cp -R /tmp/gmsh-*${GMSH}-Windows64/tutorials/* onelab-Windows64/tutorials/gmsh/
  cp -R /tmp/getdp-*${GETDP}-Windows64/templates onelab-Windows64/
  rm -rf /tmp/gmsh-*
  rm -rf /tmp/getdp-*
  rm -rf /tmp/conveks-*
  zip -r onelab-Windows64.zip onelab-Windows64
  rm -rf onelab-Windows64
  scp onelab-Windows64.zip geuzaine@onelab.info:/onelab_files/
fi

if [ $# -lt 1 ] || [ $1 == "linux" ]; then
  rm -rf onelab-Linux64*
  mkdir onelab-Linux64
  curl -O https://gmsh.info/bin/Linux/gmsh-${GMSH}-Linux64.tgz
  curl -O https://getdp.info/bin/Linux/getdp-${GETDP}-Linux64c.tgz
  ##curl -O https://getdp.info/bin/Linux/getdp-${GETDP}-Linux64r.tgz
  curl -O https://onelab.info/conveks/bin/conveks-${CONVEKS}-Linux64.zip
  mv gmsh-${GMSH}-Linux64.tgz /tmp
  mv getdp-${GETDP}-Linux64c.tgz /tmp
  ##mv getdp-${GETDP}-Linux64r.tgz /tmp
  mv conveks-${CONVEKS}-Linux64.zip /tmp
  tar zxvf /tmp/gmsh-${GMSH}-Linux64.tgz -C /tmp
  tar zxvf /tmp/getdp-${GETDP}-Linux64c.tgz -C /tmp
  ##tar zxvf /tmp/getdp-${GETDP}-Linux64r.tgz -C /tmp
  unzip -q -o /tmp/conveks-${CONVEKS}-Linux64.zip -d /tmp
  cp /tmp/README.txt onelab-Linux64
  mv /tmp/gmsh-*${GMSH}-Linux64/bin/gmsh onelab-Linux64
  mv /tmp/gmsh-*${GMSH}-Linux64/bin/onelab.py onelab-Linux64
  mv /tmp/getdp-*${GETDP}-Linux64/bin/getdp onelab-Linux64
  mv /tmp/conveks-*${CONVEKS}-Linux64/lib/conveks.py onelab-Linux64
  cp /tmp/conveks-*${CONVEKS}-Linux64/lib/libconveks*.so onelab-Linux64
  mv /tmp/gmsh-*${GMSH}-Linux64/share/doc/gmsh/LICENSE.txt onelab-Linux64/LICENSE.txt
  echo "\n\n" >> onelab-Linux64/LICENSE.txt
  cat /tmp/getdp-*${GETDP}-Linux64/share/doc/getdp/LICENSE.txt >> onelab-Linux64/LICENSE.txt
  mv /tmp/gmsh-*${GMSH}-Linux64/share/doc/gmsh/CREDITS.txt onelab-Linux64/CREDITS.txt
  echo "\n\n" >> onelab-Linux64/CREDITS.txt
  cat /tmp/getdp-*${GETDP}-Linux64/share/doc/getdp/CREDITS.txt >> onelab-Linux64/CREDITS.txt
  cp -R /tmp/models onelab-Linux64/
  cp -R /tmp/tutorials onelab-Linux64/
  cp -R /tmp/gmsh-*${GMSH}-Linux64/share/doc/gmsh/tutorials/* onelab-Linux64/tutorials/gmsh/
  cp -R /tmp/getdp-*${GETDP}-Linux64/share/doc/getdp/templates onelab-Linux64
  rm -rf /tmp/gmsh-*
  rm -rf /tmp/getdp-*
  rm -rf /tmp/conveks-*
  zip -r onelab-Linux64.zip onelab-Linux64
  rm -rf onelab-Linux64
  scp onelab-Linux64.zip geuzaine@onelab.info:/onelab_files/
fi

if [ $# -lt 1 ] || [ $1 == "macosx" ]; then
  rm -rf onelab-MacOSX*
  mkdir onelab-MacOSX
  curl -O https://gmsh.info/bin/macOS/gmsh-${GMSH}-MacOSX.dmg
  curl -O https://getdp.info/bin/macOS/getdp-${GETDP}-MacOSXc.tgz
  ##curl -O https://getdp.info/bin/macOS/getdp-${GETDP}-MacOSXr.tgz
  curl -O https://onelab.info/conveks/bin/conveks-${CONVEKS}-MacOSX.zip
  mv gmsh-${GMSH}-MacOSX.dmg /tmp
  mv getdp-${GETDP}-MacOSXc.tgz /tmp
  ##mv getdp-${GETDP}-MacOSXr.tgz /tmp
  mv conveks-${CONVEKS}-MacOSX.zip /tmp
  hdiutil convert /tmp/gmsh-${GMSH}-MacOSX.dmg -format UDTO -o /tmp/gmsh-tmp.cdr
  hdiutil attach -nobrowse -noverify -noautoopen -mountpoint gmsh_mount /tmp/gmsh-tmp.cdr
  tar zxvf /tmp/getdp-${GETDP}-MacOSXc.tgz -C /tmp
  ##tar zxvf /tmp/getdp-${GETDP}-MacOSXr.tgz -C /tmp
  unzip -q -o /tmp/conveks-${CONVEKS}-MacOSX.zip -d /tmp
  cp /tmp/README.txt onelab-MacOSX
  echo "
(The gmsh and getdp executables as well as the libraries and module files
are located in the .app bundle, i.e. in Gmsh.app/Contents/MacOS/.)" >> onelab-MacOSX/README.txt
  cp -R gmsh_mount/Gmsh.app onelab-MacOSX/
  mv /tmp/getdp-*${GETDP}-MacOSX/bin/getdp onelab-MacOSX/Gmsh.app/Contents/MacOS/
  mv /tmp/conveks-*${CONVEKS}-MacOSX/lib/conveks.py onelab-MacOSX/Gmsh.app/Contents/MacOS/
  cp /tmp/conveks-*${CONVEKS}-MacOSX/lib/libconveks*.dylib onelab-MacOSX/Gmsh.app/Contents/MacOS/
  security unlock-keychain -p ${KEYCHAIN} ${HOME}/Library/Keychains/login.keychain
  codesign -v --force --deep --options runtime --sign "Developer ID Application: Christophe Geuzaine" onelab-MacOSX/Gmsh.app
  cp gmsh_mount/LICENSE.txt onelab-MacOSX/LICENSE.txt
  echo "\n\n" >> onelab-MacOSX/LICENSE.txt
  cat /tmp/getdp-*${GETDP}-MacOSX/share/doc/getdp/LICENSE.txt >> onelab-MacOSX/LICENSE.txt
  cp gmsh_mount/CREDITS.txt onelab-MacOSX/CREDITS.txt
  echo "\n\n" >> onelab-MacOSX/CREDITS.txt
  cat /tmp/getdp-*${GETDP}-MacOSX/share/doc/getdp/CREDITS.txt >> onelab-MacOSX/CREDITS.txt
  cp -R /tmp/models onelab-MacOSX/
  cp -R /tmp/tutorials onelab-MacOSX/
  cp -R gmsh_mount/tutorials/* onelab-MacOSX/tutorials/gmsh/
  cp -R /tmp/getdp-*${GETDP}-MacOSX/share/doc/getdp/templates onelab-MacOSX/
  hdiutil eject gmsh_mount
  rm -rf /tmp/gmsh-*
  rm -rf /tmp/getdp-*
  rm -rf /tmp/conveks-*
  # cannot use zip file: it destroys the signature for onelab.py
  hdiutil create -srcfolder onelab-MacOSX onelab-MacOSX.dmg
  xcrun notarytool submit onelab-MacOSX.dmg --key /Users/geuzaine/AuthKey_4R6P5NYF3T.p8 --key-id 4R6P5NYF3T --issuer 69a6de7c-0b3a-47e3-e053-5b8c7c11a4d1 --wait
  xcrun stapler staple onelab-MacOSX.dmg
  rm -rf onelab-MacOSX
  scp onelab-MacOSX.dmg geuzaine@onelab.info:/onelab_files/
fi

if [ $# -lt 1 ] || [ $1 == "macosarm" ]; then
  rm -rf onelab-MacOSARM*
  mkdir onelab-MacOSARM
  curl -O https://gmsh.info/bin/macOS/gmsh-${GMSH}-MacOSARM.dmg
  curl -O https://getdp.info/bin/macOS/getdp-${GETDP}-MacOSARMc.tgz
  ##curl -O https://getdp.info/bin/macOS/getdp-${GETDP}-MacOSARMr.tgz
  curl -O https://onelab.info/conveks/bin/conveks-${CONVEKS}-MacOSARM.zip
  mv gmsh-${GMSH}-MacOSARM.dmg /tmp
  mv getdp-${GETDP}-MacOSARMc.tgz /tmp
  ##mv getdp-${GETDP}-MacOSARMr.tgz /tmp
  mv conveks-${CONVEKS}-MacOSARM.zip /tmp
  hdiutil convert /tmp/gmsh-${GMSH}-MacOSARM.dmg -format UDTO -o /tmp/gmsh-tmp.cdr
  hdiutil attach -nobrowse -noverify -noautoopen -mountpoint gmsh_mount /tmp/gmsh-tmp.cdr
  tar zxvf /tmp/getdp-${GETDP}-MacOSARMc.tgz -C /tmp
  ##tar zxvf /tmp/getdp-${GETDP}-MacOSARMr.tgz -C /tmp
  unzip -q -o /tmp/conveks-${CONVEKS}-MacOSARM.zip -d /tmp
  cp /tmp/README.txt onelab-MacOSARM
  echo "
(The gmsh and getdp executables as well as the libraries and module files
are located in the .app bundle, i.e. in Gmsh.app/Contents/MacOS/.)" >> onelab-MacOSARM/README.txt
  cp -R gmsh_mount/Gmsh.app onelab-MacOSARM/
  mv /tmp/getdp-*${GETDP}-MacOSARM/bin/getdp onelab-MacOSARM/Gmsh.app/Contents/MacOS/
  mv /tmp/conveks-*${CONVEKS}-MacOSARM/lib/conveks.py onelab-MacOSARM/Gmsh.app/Contents/MacOS/
  cp /tmp/conveks-*${CONVEKS}-MacOSARM/lib/libconveks*.dylib onelab-MacOSARM/Gmsh.app/Contents/MacOS/
  security unlock-keychain -p ${KEYCHAIN} ${HOME}/Library/Keychains/login.keychain
  codesign -v --force --deep --options runtime --sign "Developer ID Application: Christophe Geuzaine" onelab-MacOSARM/Gmsh.app
  cp gmsh_mount/LICENSE.txt onelab-MacOSARM/LICENSE.txt
  echo "\n\n" >> onelab-MacOSARM/LICENSE.txt
  cat /tmp/getdp-*${GETDP}-MacOSARM/share/doc/getdp/LICENSE.txt >> onelab-MacOSARM/LICENSE.txt
  cp gmsh_mount/CREDITS.txt onelab-MacOSARM/CREDITS.txt
  echo "\n\n" >> onelab-MacOSARM/CREDITS.txt
  cat /tmp/getdp-*${GETDP}-MacOSARM/share/doc/getdp/CREDITS.txt >> onelab-MacOSARM/CREDITS.txt
  cp -R /tmp/models onelab-MacOSARM/
  cp -R /tmp/tutorials onelab-MacOSARM/
  cp -R gmsh_mount/tutorials/* onelab-MacOSARM/tutorials/gmsh/
  cp -R /tmp/getdp-*${GETDP}-MacOSARM/share/doc/getdp/templates onelab-MacOSARM/
  hdiutil eject gmsh_mount
  rm -rf /tmp/gmsh-*
  rm -rf /tmp/getdp-*
  rm -rf /tmp/conveks-*
  # cannot use zip file: it destroys the signature for onelab.py
  hdiutil create -srcfolder onelab-MacOSARM onelab-MacOSARM.dmg
  xcrun notarytool submit onelab-MacOSARM.dmg --key /Users/geuzaine/AuthKey_4R6P5NYF3T.p8 --key-id 4R6P5NYF3T --issuer 69a6de7c-0b3a-47e3-e053-5b8c7c11a4d1 --wait
  xcrun stapler staple onelab-MacOSARM.dmg
  rm -rf onelab-MacOSARM
  scp onelab-MacOSARM.dmg geuzaine@onelab.info:/onelab_files/
fi
