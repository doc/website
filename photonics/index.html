<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>

<head>

<title>ONELAB Photonics</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="keywords" content="free, finite element, fem, interface, gmsh, getdp">
<meta name="viewport" content="width=device-width">
<meta name="apple-itunes-app" content="app-id=845930897">
<link href="/onelab.css" rel="stylesheet" type="text/css">
<style type="text/css"><!--
  div.small { font-size:80%; }
  ul.small { margin-top:1ex; margin-bottom:1ex; }
--></style>

</head>

<body>

<h1 class="short">ONELAB Photonics</h1>

<div id="banner">
  <img src="Diffraction-gratings_screenshot2.png" alt="">
  <img src="grating3D_skew.png" alt="">
  <img src="grating3D_solar.png" alt="">
  <img src="NonLinearEVP.png" alt="">
  <img src="rhombus.png" alt="">
  <img src="scattering_splitring.png" alt="">
</div>

<h1>Open Source Finite Element Software for Photonics Applications</h1>

<p>
  ONELAB Photonics is a set of models combining the open source finite
  element solver <a href="https://getdp.info">GetDP</a> with the open source pre-
  and post-processor <a href="https://gmsh.info">Gmsh</a> to solve photonics
  applications<a href="#1"><sup>1</sup></a>.
</p>
<p>
  These models can be used as-is for parametric studies or as template models
  since implementing new opto-geometric parameters using Gmsh and GetDP is
  rather simple.
</p>
<p>
  For instance, it is possible to compute direct problems such as the
  diffraction of a plane wave by a grating<a href="#2"><sup>2-4</sup></a> (in 2D
  and 3D) or the scattering of an arbitrary wave by a scatterer
  (T-matrix<a href="#5"><sup>5</sup></a>, near and far field data...)
</p>
<p>
  A collection of eigenvalue problems is also available, such as the
  Quasi-Normal Modes of open structures<a href="#6"><sup>6</sup></a>, the the
  Bloch band diagram of photonics crystals, the leaky modes of a microstructured
  fiber<a href="#7"><sup>7</sup></a>, or the modes resulting from non-linear
  eigenvalue problems arising when considering frequency-dispersive
  permittivities<a href="#8"><sup>8-9</sup></a>.
</p>

<h2>Quick start</h2>
<ol>
  <li>Download the <a href="/#Download">precompiled ONELAB
  software bundle</a> for Windows, Linux or MacOS.
  <li>Launch the app <img src="https://gmsh.info/gallery/icon.png" height=20px>
  <li>Open e.g. <code>models/BlochPeriodicWaveguides/rhombus.pro</code>.
  <li>Press <code>Run</code>
</ol>

<h2>Template models</h2>
<ul>
  <li>2D and 3D grating models<a href="#2"><sup>2-4</sup></a> are available
    in <code><a href="https://gitlab.onelab.info/doc/models/-/wikis/Diffraction-gratings"
    >models/DiffractionGratings</a></code>.
  <li>A general 3D scattering model<a href="#5"><sup>5</sup></a> is available
    in <code><a href="https://gitlab.onelab.info/doc/models/-/tree/master/ElectromagneticScattering"
    >models/ElectromagneticScattering</a></code>.
  <li>A model for the computation of the Bloch dispersion relation in conical
    mounts<a href="#7"><sup>7</sup></a> is avalable
    in <code><a href="https://gitlab.onelab.info/doc/models/-/wikis/Bloch-modes-in-periodic-waveguides"
    >models/BlochPeriodicWaveguides</a></code>.
  <li>A collection of non-Linear eigenvalue
    problems<a href="#8"><sup>8-9</sup></a> (quadratic, polynomial and rational)
    (for the computation of Quasi Normal Modes) is avaiable in
    <code><a href="https://gitlab.onelab.info/doc/models/-/tree/master/NonLinearEVP"
    >models/NonLinearEVP</a></code>.
  <li>An example of (Dispersive) Quasi Normal Modes expansion 
    <a href="#10"><sup>10</sup></a> is avaiable in
    <code><a href="https://gitlab.onelab.info/doc/models/-/tree/master/QuasiNormalModeExpansion"
    >models/QuasiNormalModeExpansion</a></code>.
</ul>

<h2>References</h2>

<div class="small">
  <ol class="small">
    <li><a name="1"></a>G. Demésy, A. Nicolet, F. Zolla,
      C. Geuzaine. <a href="https://doi.org/10.1051/photon/202010040">Modélisation
        par la méthode de éléments finis avec ONELAB</a>. Photoniques 100, 40-45,
      2020.
    <li><a name="2"></a>G. Demésy, F. Zolla, A. Nicolet, M. Commandré.
      <a href="https://doi.org/10.1364/JOSAA.27.000878">
        All-purpose finite element formulation for arbitrarily shaped
        crossed-gratings embedded in a multilayered stack</a>.  JOSA A 27.4,
      878-889, 2010.
    <li><a name="3"></a>G. Demésy, F. Zolla, A. Nicolet.
      <a href="https://arxiv.org/abs/1710.11451">
        A ONELAB model for the parametric study of mono-dimensional diffraction
        gratings</a>.  arXiv:1710.11451.
    <li><a name="4"></a>G. Demésy, S. John.
      <a href=" https://doi.org/10.1063/1.4752775">
        Solar energy trapping with modulated silicon nanowire photonic crystals</a>.
      Journal of Applied Physics 112.7, 074326, 2012.
    <li><a name="5"></a>G. Demésy, J.-C. Auger, B. Stout.
      <a href="https://arxiv.org/abs/1807.02355">
        Scattering matrix of arbitrarily shaped objects: combining finite
        elements and vector partial waves</a>.  JOSA A 35.8 1401-1409, 2018.
    <li><a name="6"></a>N. Marsic, H. De Gersem, G. Demésy, A. Nicolet, C. Geuzaine.
      <a href="https://iopscience.iop.org/article/10.1088/1367-2630/aab6fd">
        Modal analysis of the ultrahigh finesse Haroche QED cavity</a>.
      New Journal of Physics 20.4, 043058, 2018.
    <li><a name="7"></a>F. Zolla, G. Renversez, A. Nicolet.  Foundations of
      photonic crystal fibres. World Scientific, 2005.
    <li><a name="8"></a>G. Demésy, A. Nicolet, B. Gralak, C. Geuzaine,
      C. Campos, J. E. Roman.
      <a href="https://arxiv.org/abs/1802.02363">
        Non-linear eigenvalue problems with GetDP and SLEPc: Eigenmode
        computations of frequency-dispersive photonic open structures</a>.
      arXiv:1802.02363.
    <li><a name="9"></a>F. Zolla, A. Nicolet, G. Demésy,
      <a href="https://arxiv.org/abs/1807.02355">
        Photonics in highly dispersive media: the exact modal expansion</a>.
      Opt. Lett. 43, 5813, 2018.
    <li><a name="10"></a>A. Nicolet, G. Demésy, F. Zolla, C. Campos, J. E. Roman, C. Geuzaine,
      <a href="https://www.sciencedirect.com/science/article/abs/pii/S099775382200239X">
        Physically agnostic quasi normal mode expansion in time dispersive structures: 
        From mechanical vibrations to nanophotonic resonances</a>.
        European J. of Mechanics - A/Solids, 104809, 2022.
  </ol>
</div>

<h2>Sponsors</h2>

<p>
  ONELAB Photonics was funded in part by the French Agence Nationale pour la
  recherche (ANR-16-CE24-0013), the Walloon Region (WIST3 No 1017086 ONELAB) and
  the Belgian French Community (ARC WAVES 15/19-03).
</p>

<center style="margin-top:4ex;margin-bottom:4ex">
  <a href="http://www.fresnel.fr"><img src="/logo_fresnel.jpg" height="60px"></a>&nbsp;
  <a href="http://www.fresnel.fr"><img src="/logo_amu.jpg" height="50px"></a>&nbsp;&nbsp;&nbsp;
  <a href="http://www.fresnel.fr"><img src="/logo_anr.png" height="40px"></a>&nbsp;
  <a href="http://www.uliege.be"><img src="/logo_uliege.jpg" height="60px"></a>&nbsp;
  <a href="http://www.wallonie.be"><img src="/logo_rw.jpg" height="60px"></a>
</center>

</body>
</html>
