<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>

<head>

<title>GetDDM</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="keywords" content="free, finite element, fem, interface, gmsh, getdp">
<meta name="viewport" content="width=device-width">
<meta name="apple-itunes-app" content="app-id=845930897">
<link href="/onelab.css" rel="stylesheet" type="text/css">
<style type="text/css"><!--
  div.small { font-size:80%; }
  ul.small { margin-top:1ex; margin-bottom:1ex; }
--></style>

</head>

<body>

<h1 class="short">GetDDM</h1>

<div id="banner">
  <img src="submarine.png" alt="">
  <img src="falcon_field.png" alt="">
  <img src="falcon_partitions.png" alt="">
  <img src="cobra.png" alt="">
  <img src="marmousi.png" alt="">
</div>

<h1>An Open Framework for Testing Optimized Schwarz Methods for Time-Harmonic
Wave Problems</h1>

<p>
  GetDDM<a href="#1"><sup>1</sup></a> combines <a href="https://getdp.info">GetDP</a>
  and <a href="https://gmsh.info">Gmsh</a> to solve large scale finite element
  problems using optimized Schwarz domain decomposition methods.
</p>
<p>
  <a href="https://gitlab.onelab.info/doc/models/wikis/Domain-decomposition-methods-for-waves">Examples
  for time-harmonic acoustic and electromagnetic wave problems</a> implement
  several families of transmission conditions: zeroth- and second-order
  optimized conditions<a href="#2"><sup>2-7</sup></a>, Padé-localized
  square-root conditions<a href="#8"><sup>8-9</sup></a> and PML
  conditions<a href="#10"><sup>10</sup></a>. Several variants of the
  double-sweep preconditioner<a href="#10"><sup>10</sup></a> are also
  implemented.
</p>
<p>
  For more information about these methods as well as the implementation, please
  refer
  to <a href="https://people.montefiore.uliege.be/geuzaine/preprints/getddm_preprint.pdf">GetDDM:
  an Open Framework for Testing Optimized Schwarz Methods for Time-Harmonic Wave
  Problems</a>.
</p>

<h2>Quick start</h2>

<ol>
  <li>Download the <a href="/#Download">precompiled ONELAB
  software bundle</a> for Windows, Linux or macOS.
  <li>Launch the app <img src="https://gmsh.info/gallery/icon.png" height=20px>
  <li>Open <code>models/GetDDM/main.pro</code>
  <li>Press <code>Run</code>
</ol>

<h2>Parallel computations</h2>

<ol>
  <li>Download the <a href="/files/onelab-source.zip">ONELAB
      source code</a>
  <li><a href="https://gitlab.onelab.info/getdp/getdp/wikis/GetDP-compilation">Compile
  GetDP</a>
  and <a href="https://gitlab.onelab.info/gmsh/gmsh/wikis/Gmsh-compilation">Gmsh</a>
  with MPI support
  <li>Run the models on a computer cluster with MPI, e.g. the <code>waveguide3d</code>
    model on 100 CPUs, using:
    <pre>mpirun -np 100 gmsh -setnumber N_DOM 100 waveguide3d.geo -
mpirun -np 100 getdp -setnumber N_DOM 100 waveguide3d.pro -solve DDM</pre>
</ol>
<p>
  The actual commands will depend on your particular MPI setup. Sample scripts
  for <a href="https://gitlab.onelab.info/doc/models/tree/master/GetDDM/run_slurm.sh">SLURM</a>
  and <a href="https://gitlab.onelab.info/doc/models/tree/master/GetDDM/run_pbs.sh">PBS</a>
  schedulers are provided.
</p>

<h2>References</h2>

<div class="small">
  <ol class="small">
    <li><a name="1"></a>B. Thierry, A.Vion, S. Tournier, M. El Bouajaji,
      D. Colignon, N. Marsic, X. Antoine,
      C. Geuzaine. <a href="https://people.montefiore.uliege.be/geuzaine/preprints/getddm_preprint.pdf">GetDDM:
      an Open Framework for Testing Optimized Schwarz Methods for Time-Harmonic
      Wave Problems</a>.  Computer Physics Communications 203, 309-330, 2016.
    <li><a name="2"></a>B. Després, Méthodes de Décomposition de Domaine pour les
      Problèmes de Propagation d'Ondes en Régime Harmonique. Le Théorème de Borg
      pour l'Equation de Hill Vectorielle, PhD Thesis, Paris VI University,
      France, 1991.
    <li><a name="3"></a>B. Després, P. Joly and J. Roberts, A domain decomposition
      method for the harmonic Maxwell equations, Iterative methods in linear
      algebra (Brussels, 1991), pp. 475-484, North-Holland, 1992.
    <li><a name="4"></a>M. Gander, F. Magoulès and F. Nataf, Optimized Schwarz methods without
      overlap for the Helmholtz equation}, SIAM Journal on Scientific Computing,
      24(1), pp. 38-60, 2002.
    <li><a name="5"></a>V. Dolean, M. Gander and L. Gerardo-Giorda, Optimized
      Schwarz methods for Maxwell's equations, SIAM Journal on Scientific
      Computing, 31(3), pp. 2193-2213, 2009.
    <li><a name="6"></a>A. Bendali and Y. Boubendir, Non-Overlapping Domain
      Decomposition Method for a Nodal Finite Element Method, Numerische
      Mathematik 103(4), pp.515-537, (2006).
    <li><a name="7"></a>V. Rawat and J.-F. Lee, Nonoverlapping Domain Decomposition
      with Second Order Transmission Condition for the Time-Harmonic Maxwell's
      Equations, SIAM Journal on Scientific Computing, 32(6), pp. 3584-3603,
      2010.
    <li><a name="8"></a>Y. Boubendir, X. Antoine and
      C. Geuzaine. <a href="https://people.montefiore.uliege.be/geuzaine/preprints/ddm_helmholtz_preprint.pdf">A
      quasi-optimal non-overlapping domain decomposition algorithm for the
      Helmholtz equation</a>.  Journal of Computational Physics 231 (2),
      262-280, 2012.
    <li><a name="9"></a>M. El Bouajaji, X. Antoine and
      C. Geuzaine. <a href="https://people.montefiore.uliege.be/geuzaine/preprints/osrc_maxwell_preprint.pdf">Approximate
      local magnetic-to-electric surface operators for time-harmonic Maxwell's
      equations</a>.  Journal of Computational Physics 279 241-260, 2014.
    <li><a name="10"></a>A. Vion and
      C. Geuzaine. <a href="https://people.montefiore.uliege.be/geuzaine/preprints/ddm_double_sweep_preprint.pdf">
      Double sweep preconditioner for optimized Schwarz methods applied to the
      Helmholtz problem</a>.  Journal of Computational Physics 266, 171-190,
      2014.
  </ol>
</div>

<h2>Sponsors</h2>

<p>
  GetDDM development was funded in part by the Belgian Science Policy (IAP P6/21
  and P7/02), the Belgian French Community (ARC 09/14-02 and 15/19-03), the
  Walloon Region (WIST3 No 1017086 ONELAB and ALIZEES), the Agence Nationale
  pour la Recherche (ANR-09-BLAN-0057-01 MicroWave) and the EADS Foundation
  (grant 089-1009-1006 High-BRID).
</p>


<center style="margin-top:4ex;margin-bottom:4ex">
  <a href="http://www.uliege.be"><img src="/logo_uliege.jpg" height="68px"></a>&nbsp;
  <a href="http://www.univ-lorraine.fr"><img src="logo_universite_de_lorraine.png" height="68px"></a>&nbsp;
  <a href="http://www.wallonie.be"><img src="/logo_rw.jpg" height="68px"></a>&nbsp;
  <a href="http://www.belspo.be"><img src="/logo_belspo.jpg" height="68px"></a>&nbsp;
  <a href="http://www.airbusgroup.com/"><img src="logo_ANR.gif" height="68px"></a>&nbsp;
  <a href="http://www.airbusgroup.com/"><img src="logo_Fondation_AirbusGroup.jpg" height="68px"></a>
</center>

</body>
</html>
