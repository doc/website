<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>

<head>

<title>ONELAB: Open Numerical Engineering LABoratory</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="keywords" content="free, finite element, fem, interface, gmsh, getdp">
<meta name="viewport" content="width=device-width">
<meta name="apple-itunes-app" content="app-id=845930897">
<link href="onelab.css" rel="stylesheet" type="text/css">
<link href="favicon.ico" rel="shortcut icon" type="image/x-icon">
<style type="text/css"><!--
  div.small { font-size:80%; }
--></style>

</head>

<body>

<h1 class="short">ONELAB</h1>

<h1>Open Numerical Engineering LABoratory
  <a href="https://www.twitter.com/OnelabModels"><img class="social"
     src="twitter.png" alt="twitter"></a>
</h1>

<div><span style="white-space:nowrap"><!--
--><a href="https://gitlab.onelab.info/doc/models/wikis/Inductor"><!--
--><img width="25%" src="small_inductor.png"></a><!--
--><a href="https://gitlab.onelab.info/doc/models/wikis/Magnetometer"><!--
--><img width="25%" src="small_magnetometer.png"></a><!--
--><a href="https://gitlab.onelab.info/doc/models/wikis/Electric-machines"><!--
--><img width="25%" src="small_machines.png"></a><!--
--><a href="https://gitlab.onelab.info/doc/models/wikis/Magnetodynamics-with-cohomology-conditions"><!--
--><img width="25%" src="small_indheat.png"></a><!--
--></span><br><span style="white-space:nowrap"><!--
--><a href="https://gitlab.onelab.info/doc/models/wikis/Waveguides"><!--
--><img width="25%" src="small_waveguides.png"></a><!--
--><a href="https://gitlab.onelab.info/doc/models/wikis/Electromechanical-relay"><!--
--><img width="25%" src="small_relay.png"></a><!--
--><a href="https://gitlab.onelab.info/doc/models/wikis/Bloch-modes-in-periodic-waveguides"><!--
--><img width="25%" src="small_bloch.png"></a><!--
--><a href="https://gitlab.onelab.info/doc/models/wikis/Acoustic-scattering"><!--
--><img width="25%" src="small_acoustic.png"></a><!--
--></span></div>

<p>
ONELAB is an open-source, lightweight interface to finite element software. It
is completely free: the default ONELAB software bundle contains the mesh
generator <a href="https://gmsh.info">Gmsh</a>, the finite element solver
<a href="https://getdp.info">GetDP</a> and the optimization
library <a href="conveks">con<u>v</u>eks</a>. Many other codes (free
or not) can be easily interfaced as well.
</p>

<h2>Getting started</h2>

<ol><a name="Download"></a>
  <li>Download the ONELAB software bundle:
  <ul>
    <li>Desktop version for <a href="files/onelab-Windows64.zip">Windows</a>,
      <a href="files/onelab-Linux64.zip">Linux</a>,
      <a href="files/onelab-MacOSX.dmg">macOS (x86)</a> or
      <a href="files/onelab-MacOSARM.dmg">macOS (ARM)</a> <a href="#1"><sup>*</sup></a>
    <li>Mobile version for <a href="https://play.google.com/store/apps/details?id=org.geuz.onelab">Android</a> and
      <a href="https://itunes.apple.com/us/app/onelab/id845930897">iOS</a>
    <li><a href="files/onelab-source.zip">Source code</a>
  </ul>
  <li>Launch the app <img src="https://gmsh.info/gallery/icon.png" height=20px>
  <li>Open a model:
  <ul>
    <li>Desktop version: go to the <code>File/Open</code> menu and select a
    GetDP <code>.pro</code> file,
    e.g. <code>models/Magnetometer/<a href="https://gitlab.onelab.info/doc/models/blob/master/Magnetometer/magnetometer.pro">magnetometer.pro"</a></code>
    <li>Mobile version: select one of the preloaded models
  </ul>
  <li>Press <code>Run</code>
  <li>... then explore
    <a href="https://gitlab.onelab.info/doc/models/wikis">other models</a>, or
    learn how to create your own with
    <a href="https://gitlab.onelab.info/doc/tutorials/wikis">the tutorials</a>
</ol>

<div class="small">
  <a name="1"><sup>*</sup></a> Desktop versions require Windows &ge; 10, Linux
  with glibc &ge; 2.24, macOS (x86 - Intel processors) &ge; 10.15 or macOS (ARM
  - Apple M-series processors) &ge; 12
</div>

<h2>Additional information</h2>

<p>
  ONELAB can interface finite element and related software (<em>ONELAB
    clients</em>) in several ways:
</p>
<ol>
  <li>By using the ONELAB C++ library or the ONELAB Python module to communicate
    with the Gmsh ONELAB server over TCP/IP or Unix sockets. This is what is
    done by <a href="https://getdp.info">GetDP</a>; any C++ and Python code can
    do the same with minimal code modifications.
  <li>By using the Gmsh API in C++, C, Python or Julia to comunicate with the
    ONELAB server over TCP/IP or Unix sockets, or directly in-memory. This
    allows complete flexibility over how the client interacts with ONELAB
    (e.g. by pre-processing its input files) and has been successfully used with
    e.g.  <a href="https://www.csc.fi/web/elmer">Elmer</a>,
    <a href="https://www.openfoam.com/">OpenFOAM</a>,
    <a href="https://www.code-aster.org/">Code_Aster</a>,
    <a href="https://www.3ds.com/fr/produits-et-services/simulia/produits/abaqus">Abaqus</a>,
    <a href="http://www.calculix.de">CalculiX</a>,
    <a href="https://www.uee.uliege.be/cms/c_6331644/fr/uee-safir">Safir</a>, ...
</ol>
<p>
  The ONELAB interface allows to interact with clients and have them share
  parameters and modeling information.  Contrary to most solver interfaces, the
  ONELAB server has no <em>a priori</em> knowledge about any specifics (input
  file format, syntax, ...) of the clients. This is made possible by having any
  simulation preceded by an analysis phase, during which the clients are asked
  to upload their parameter set to the server. The issues of completeness and
  consistency of the parameter sets are completely dealt with on the client
  side: the role of ONELAB is limited to data centralization, modification and
  re-dispatching.
</p>
<p>
  See this <a href="slides/onelab.pdf">general presentation</a> for more
  information and
  the <a href="https://gitlab.onelab.info/doc/tutorials/wikis">tutorials</a> for
  examples.
</p>

<h2>Featured models</h2>

<ul>
  <li><a href="http://www.life-hts.uliege.be">Life-HTS</a> for solving problems with
    high-temperature superconductors
  <li><a href="https://onelab.info/photonics">ONELAB-Photonics</a> for photonics applications
  <li><a href="https://onelab.info/GetDDM">GetDDM</a> for optimized domain decomposition
    methods to solve large scale time-harmonic acoustic and electromagnetic
    wave problems
  <li><a href="https://gitlab.onelab.info/doc/models/wikis/Electric-machines">Electrical
      machines</a> for modelling electrical machines
</ul>

<h2>Sponsors</h2>

<p>
  The ONELAB project was funded by the Walloon Region under
  <a href="http://recherche-technologie.wallonie.be/projets/index.html?IDD=17018">WIST3
    grant n° 1017086</a> and <em>Fonds de maturation</em> grant n° 1217742
  (AWE), and was sponsored by <a href="http://www.gdtech.eu">GDTech</a>
  and <a href="http://www.audaxis.com">Audaxis</a>. Additional funding has
  been provided by <a href="http://www.belspo.be">the Belgian Science
    Policy</a> under grant IAP P7/02.
</p>

<center style="margin-top:4ex;margin-bottom:4ex">
  <a href="http://www.uliege.be"><img src="logo_uliege.jpg" height="68px"></a>&nbsp;
  <a href="http://www.uclouvain.be"><img src="logo_ucl.jpg" height="68px"></a>&nbsp;
  <a href="http://www.wallonie.be"><img src="logo_rw.jpg" height="68px"></a>&nbsp;
  <a href="http://www.belspo.be"><img src="logo_belspo.jpg" height="68px"></a>&nbsp;
  <a href="http://www.gdtech.eu"><img src="logo_gdtech.jpg" height="68px"></a>&nbsp;
  <a href="http://www.audaxis.be"><img src="logo_audaxis.png" height="68px"></a>
</center>

</body>
</html>
